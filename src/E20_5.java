/**
 * Written by: Yik Haw Teoh
 * Description: Main function to display panel frame
 */

import javax.swing.*;

public class E20_5 {
    /**
     * Displays panel frame
     * @param args
     */
    public static void main(String args[]){
        JFrame frame= new PanelFrame();

        frame.setSize(375, 400);
        frame.setTitle("BG Color Changer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

