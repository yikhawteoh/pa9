/**
 * Written by: Yik Haw Teoh
 * Description: RectangleFrame that displays random amount of rectangles and increases or decreases in number depending on user input
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class RectangleFrame extends JFrame {
    public static final int FRAME_WIDTH = 500;
    public static final int FRAME_HEIGHT = 500;
    private int mRectangleCount = 1;
    private JButton buttonMore;
    private JButton buttonLess;
    ArrayList<RectangleComponent> rectangleComponents = new ArrayList<RectangleComponent>();

    /**
     * RectangleFrame constructor, prepares buttons and creates control panel
     */
    public RectangleFrame(){
        buttonMore = new JButton("More");
        buttonLess = new JButton("Fewer");
        createControlPanel();
        makeRectangles();
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
    }

    /**
     * More button listener
     */
    class ClickListenerMore implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            //Listener method accesses buttonMore and doubles mRectangleCount when executed
            if(mRectangleCount==0) mRectangleCount = 1;
            mRectangleCount = mRectangleCount*2;
            //Clear frame
            clear();
            //Make relevant number of rectangles based on mRectangleCount
            makeRectangles();
            //Repaints frame
            repaint();
        }
    }

    /**
     * Fewer button listener
     */
    class ClickListenerLess implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            //Listener method accesses buttonLess and halves mRectangleCount when executed
            mRectangleCount = mRectangleCount/2;
            clear();
            makeRectangles();
            repaint();
        }
    }

    /**
     * Adds buttons and their respective listeners to the control panel, that is added to the frame
     */
    public void createControlPanel(){
        //Add action listener to button class
        ActionListener listenerMore = new ClickListenerMore();
        buttonMore.addActionListener(listenerMore);
        ActionListener listenerLess = new ClickListenerLess();
        buttonLess.addActionListener(listenerLess);
        JPanel rectangleSelection = new JPanel();
        rectangleSelection.setLayout(new GridLayout(1,2));
        //Add buttons to frame
        rectangleSelection.add(buttonMore);
        rectangleSelection.add(buttonLess);
        add(rectangleSelection,BorderLayout.SOUTH);
    }

    /**
     * Clears all rectangles on frame and in array
     */
    public void clear(){
        for (RectangleComponent r : rectangleComponents) {
            this.remove(r);
        }
        rectangleComponents.clear();

        SwingUtilities.updateComponentTreeUI(this);
    }

    /**
     * Makes necessary amount of rectangles and updates the frame when done
     */
    public void makeRectangles() {
        Random rand = new Random();
        RectangleComponent tempRectangle;
        for(int i=0; i<mRectangleCount;i++){
            int width = rand.nextInt(100);
            int height = rand.nextInt(100);
            int xLeft = rand.nextInt(FRAME_WIDTH-width);
            int yTop = rand.nextInt(FRAME_HEIGHT-height);
            tempRectangle = new RectangleComponent(width, height, xLeft, yTop);
            //add tempRectangle into array
            rectangleComponents.add(tempRectangle);
            //add tempRectangle to frame
            add(tempRectangle);
            //Update frame
            SwingUtilities.updateComponentTreeUI(this);
        }
    }

    //Getters and setters

    public int getmRectangleCount() {
        return mRectangleCount;
    }

    public void setmRectangleCount(int mRectangleCount) {
        this.mRectangleCount = mRectangleCount;
    }

    public JButton getButtonMore() {
        return buttonMore;
    }

    public void setButtonMore(JButton buttonMore) {
        this.buttonMore = buttonMore;
    }

    public JButton getButtonLess() {
        return buttonLess;
    }

    public void setButtonLess(JButton buttonLess) {
        this.buttonLess = buttonLess;
    }

    public ArrayList<RectangleComponent> getRectangleComponents() {
        return rectangleComponents;
    }

    public void setRectangleComponents(ArrayList<RectangleComponent> rectangleComponents) {
        this.rectangleComponents = rectangleComponents;
    }
}
