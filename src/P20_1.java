/**
 * Written by: Yik Haw Teoh
 * Description: Main function to display better rectangle frame
 */

import javax.swing.*;

public class P20_1 {
    public static void main(String args[]){
        JFrame frame= new BetterRectangleFrame();

        frame.setSize(500, 500);
        frame.setTitle("Rectangles");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
