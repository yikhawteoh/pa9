/**
 * Written by: Yik Haw Teoh
 * Description: Panel frame that changes color based on user selection from combo box
 */

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Extends JFrame that would be displayed
 */
public class PanelFrame extends JFrame{
    public static final int FRAME_WIDTH = 500;
    public static final int FRAME_HEIGHT = 500;
    private JPanel colorPanel;
    JComboBox colorCombo = new JComboBox();

    /**
     * PanelFrame constructor, add panels and set up control panel
     */
    public PanelFrame(){
        colorPanel = new JPanel();
        add(colorPanel, BorderLayout.CENTER);
        createControlPanel();
        setBackgroundColor();
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
    }

    /**
     * Action listener for combo box
     */
    class ChoiceListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            setBackgroundColor();
        }
    }

    /**
     * Make control panel by adding selections into combo box, and adding combo box into a JPanel that is added to the frame
     */
    public void createControlPanel(){
        ActionListener listener = new ChoiceListener();

        colorCombo.addItem("Red");
        colorCombo.addItem("Green");
        colorCombo.addItem("Blue");
        colorCombo.addActionListener(listener);
        JPanel colorSelection = new JPanel();
        colorSelection.add(colorCombo);

        add(colorSelection,BorderLayout.SOUTH);
    }

    /**
     * Event that is called whrn action listener is triggered
     */
    public void setBackgroundColor(){
        String colorName = (String) colorCombo.getSelectedItem();

        switch(colorName){
            case "Red":{
                colorPanel.setBackground(Color.RED);
                colorPanel.repaint();
                break;
            }
            case "Green":{
                colorPanel.setBackground(Color.GREEN);
                colorPanel.repaint();
                break;
            }
            case "Blue":{
                colorPanel.setBackground(Color.BLUE);
                colorPanel.repaint();
                break;
            }
            default:{
                System.out.println("Unknown input");
            }
        }
    }

    //Getters and setters
    public JPanel getColorPanel() {
        return colorPanel;
    }

    public void setColorPanel(JPanel colorPanel) {
        this.colorPanel = colorPanel;
    }

    public JComboBox getColorCombo() {
        return colorCombo;
    }

    public void setColorCombo(JComboBox colorCombo) {
        this.colorCombo = colorCombo;
    }
}
