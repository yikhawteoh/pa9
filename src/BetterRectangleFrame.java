/**
 * Written by: Yik Haw Teoh
 * Description: RectangleFrame that displays random amount of rectangles and increases or decreases in number depending on user input with a slider
 */

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class BetterRectangleFrame extends JFrame {
    public static final int FRAME_WIDTH = 500;
    public static final int FRAME_HEIGHT = 500;
    private JSlider rectangleSlider;
    private JPanel rectanglesPanel;
    ArrayList<RectangleComponent> rectangleComponents = new ArrayList<RectangleComponent>();

    /**
     * BetterRectangleFrame constructor, creates control panel
     */
    public BetterRectangleFrame(){
        createControlPanel();
        makeRectangles();
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
    }

    /**
     * Slider listener
     */
    class CountListener implements ChangeListener {
        public void stateChanged(ChangeEvent event) {
            //Listener method accesses slider and makes given number of rectangles when executed
            //clear();
            makeRectangles();
            repaint();
        }
    }

    /**
     * Adds slider and the respective listener to the control panel, that is added to the frame
     */
    public void createControlPanel(){
        //Add change listener to rectangleSlider class
        ChangeListener listener = new CountListener();
        rectangleSlider = new JSlider(0,50,0);
        rectangleSlider.addChangeListener(listener);
        JPanel controlPanel = new JPanel();
        controlPanel.add(new JLabel("Rectangle count"));
        controlPanel.add(rectangleSlider);
        add(controlPanel,BorderLayout.SOUTH);
    }

    /**
     * Clears all rectangles on frame and in array
     */
    public void clear(){
        for (RectangleComponent r : rectangleComponents) {
            this.remove(r);
        }
        rectangleComponents.clear();

        SwingUtilities.updateComponentTreeUI(this);
    }

    /**
     * Makes necessary amount of rectangles and updates the frame when done
     */
    public void makeRectangles() {
        Random rand = new Random();
        RectangleComponent tempRectangle;
        int mRectangleCount = rectangleSlider.getValue();
        for(int i=0; i<mRectangleCount;i++){
            int width = rand.nextInt(100);
            int height = rand.nextInt(100);
            int xLeft = rand.nextInt(FRAME_WIDTH-width);
            int yTop = rand.nextInt(FRAME_HEIGHT-height);
            tempRectangle = new RectangleComponent(width, height, xLeft, yTop);
            //add tempRectangle into array
            rectangleComponents.add(tempRectangle);
            //add tempRectangle to frame
            this.add(tempRectangle);
            //Update frame
            SwingUtilities.updateComponentTreeUI(this);

        }
        SwingUtilities.updateComponentTreeUI(this);
        //System.out.println("Count: "+mRectangleCount);
    }

    //Getters and setters

    public JSlider getRectangleSlider() {
        return rectangleSlider;
    }

    public void setRectangleSlider(JSlider rectangleSlider) {
        this.rectangleSlider = rectangleSlider;
    }

    public JPanel getRectanglesPanel() {
        return rectanglesPanel;
    }

    public void setRectanglesPanel(JPanel rectanglesPanel) {
        this.rectanglesPanel = rectanglesPanel;
    }

    public ArrayList<RectangleComponent> getRectangleComponents() {
        return rectangleComponents;
    }

    public void setRectangleComponents(ArrayList<RectangleComponent> rectangleComponents) {
        this.rectangleComponents = rectangleComponents;
    }
}
