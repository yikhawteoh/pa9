/**
 * Written by: Yik Haw Teoh
 * Description: RectangleComponent that extends from JComponent that draws with graphics g
 */
import javax.swing.*;
import java.awt.*;

public class RectangleComponent extends JComponent {
    private int mWidth, mHeight, mXLeft, mYTop;

    /**
     * RectangleComponent constructor, stores relevant values for rectangle class
     * @param width
     * @param height
     * @param x
     * @param y
     */
    public RectangleComponent(int width, int height, int x, int y){
        mWidth = width; mHeight = height; mXLeft = x; mYTop = y;
        //System.out.println("Width: "+width+" Height: "+height+"  ("+x+","+y+")");
    }

    /**
     * Paints rectangle at given (x,y) coordinates and width and length, uses paintComponent from super class
     * @param g
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLUE);
        g.fillRect(mXLeft,mYTop,mWidth,mHeight);
        //System.out.println("Painted "+"Width: "+mWidth+" Height: "+mHeight+"  ("+mXLeft+","+mYTop+")");
    }

    //Getters and setters

    public int getmWidth() {
        return mWidth;
    }

    public void setmWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    public int getmHeight() {
        return mHeight;
    }

    public void setmHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getmXLeft() {
        return mXLeft;
    }

    public void setmXLeft(int mXLeft) {
        this.mXLeft = mXLeft;
    }

    public int getmYTop() {
        return mYTop;
    }

    public void setmYTop(int mYTop) {
        this.mYTop = mYTop;
    }
}
