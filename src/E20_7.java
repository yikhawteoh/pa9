/**
 * Written by: Yik Haw Teoh
 * Description: Main function to display rectangle frame
 */

import javax.swing.*;

public class E20_7 {
    public static void main(String args[]){
        JFrame frame= new RectangleFrame();

        frame.setSize(500, 500);
        frame.setTitle("Rectangles");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
